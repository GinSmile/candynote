package com.xujinsmile.candynote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.agimind.widget.SlideHolder;
import com.xujinsmile.sqlite.DatabaseHelper;

public class MainActivity extends Activity {
	
	static SharedPreferences pre;
	static SharedPreferences.Editor editor;
	private SlideHolder mSlideHolder;
	ListView listView;
	DatabaseHelper dbHelper;
	SQLiteDatabase db;		
	
	public static SharedPreferences getSharedPreferences(){
		return pre;
	}
	
	public static SharedPreferences.Editor getSharedPreferencesEditor(){
		return editor;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mSlideHolder = (SlideHolder) findViewById(R.id.slideHolder);
		
		pre = this.getSharedPreferences("info", MODE_PRIVATE);	
		editor = pre.edit();
		
		Button addBtn = (Button)findViewById(R.id.addBtn);
		addBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this, AddNote.class);
				startActivity(intent);
			}
			
		});
		
		/************************************侧滑菜单键*******************************/
		Button about = (Button)findViewById(R.id.about);
		about.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub						
				mSlideHolder.close();
				Intent intent = new Intent(MainActivity.this, About.class);
				startActivity(intent);
			}
			
		});	
		
		
		
		dbHelper = new DatabaseHelper(MainActivity.this,"candy_note_db");
		db = dbHelper.getReadableDatabase();
		
		//为每一个ListView项绑定监听器，点击时触发
		ListView listView = (ListView)findViewById(R.id.list);
		listView.setOnItemClickListener(new OnItemClickListener(){
			 public void onItemClick(AdapterView<?> arg0, View arg1,  final int position, long arg3) {   
				 Cursor cursor = dbHelper.getReadableDatabase().rawQuery("select * from note", new String[]{});
				 ArrayList<Map<String, String>> list = convertCrusorToList(cursor);
				 
				 Map<String, String> map = list.get(position);
				 Toast.makeText(MainActivity.this, map.get("id"), Toast.LENGTH_SHORT).show();
				 
				 Intent intent = new Intent(MainActivity.this, EditNote.class);
	             intent.putExtra("text", map.get("text"));
	             intent.putExtra("id", map.get("id"));
	             startActivity(intent);
	             
	           }   
		});
		
		//为每一个ListView项绑定监听器，长按时触发
		listView.setOnItemLongClickListener(new ListView.OnItemLongClickListener(){
					@Override
					public boolean onItemLongClick(AdapterView<?> arg0, final View view,
							final int position, long arg3) {
						
						Cursor cursor = dbHelper.getReadableDatabase().rawQuery("select * from note", new String[]{});
						ArrayList<Map<String, String>> list = convertCrusorToList(cursor);
						final Map<String, String> map = list.get(position);
						
						final Builder builder = new AlertDialog.Builder(MainActivity.this);
						builder.setTitle("是否删除该项？");
						builder.setPositiveButton("确定", 
								new android.content.DialogInterface.OnClickListener(){
							
							@Override
							public void onClick(DialogInterface dialog, int which){
								//删除该项
								dbHelper = new DatabaseHelper(MainActivity.this,"candy_note_db",DatabaseHelper.VERSION);
								SQLiteDatabase db = dbHelper.getReadableDatabase();
								
								String id = map.get("id");
								
								db.delete("note", "id=?", new String[]{id});
								showNotes();
							}
						});
						
						builder.setNegativeButton("取消", 
								new android.content.DialogInterface.OnClickListener(){
							
							@Override
							public void onClick(DialogInterface dialog, int which){
								//do nothing
							}
						});
						
						
						builder.create().show();
						return true;
						
						
						
					}
					
		
		
	});
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		showNotes();
	}

	
	private void showNotes(){
		/********************************填充listview******************************/
		
		Cursor cursor = dbHelper.getReadableDatabase().rawQuery("select * from note", new String[]{});
		ArrayList<Map<String, String>> list = convertCrusorToList(cursor);
		
		SimpleAdapter adapter = new SimpleAdapter(
				MainActivity.this, 
				list,
				R.layout.line,
				new String[]{"text"},
				new int[]{R.id.line});
		
		ListView listView = (ListView)findViewById(R.id.list);
		listView.setAdapter(adapter);
	}
	
	
	private ArrayList<Map<String, String>> convertCrusorToList(Cursor cursor){		
		ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();
		while(cursor.moveToNext()){
			Map<String, String> map = new HashMap<String, String>();
			map.put("id", cursor.getString(0));
			map.put("date", cursor.getString(1));
			map.put("text", cursor.getString(2));
			
			result.add(map);
		}
		
		return result;
	}
	
	


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	     if(keyCode == KeyEvent.KEYCODE_MENU) {
	        // 监控菜单键
	        mSlideHolder.toggle();
	        return false;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	
	
}
