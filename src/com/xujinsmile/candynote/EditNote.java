package com.xujinsmile.candynote;

import java.util.Date;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xujinsmile.sqlite.DatabaseHelper;

public class EditNote extends Activity {
	
	DatabaseHelper dbHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_note);		
		
		EditText text = (EditText)findViewById(R.id.addNoteText);
		Intent intent = this.getIntent();
		text.setText(intent.getStringExtra("text"));
		
		
		
		Button okBtn = (Button)findViewById(R.id.okBtn);
		okBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				updateNote();
				Toast.makeText(EditNote.this, "已保存便签", Toast.LENGTH_SHORT).show();
				
			}
			
		});
		
		
	}

	private void updateNote(){
		//提取出数据
		EditText newNote = (EditText)findViewById(R.id.addNoteText);
		String newNoteStr = newNote.getText().toString();	
		
		Date date = new Date();
		
		//生成ContentValues对象
		ContentValues values = new ContentValues();
		values.put("text",newNoteStr);
		values.put("date",date.toString());
		dbHelper = new DatabaseHelper(EditNote.this,"candy_note_db",DatabaseHelper.VERSION);
		SQLiteDatabase db = dbHelper.getReadableDatabase();


		String id = this.getIntent().getStringExtra("id");
		db.update("note", values, "id=?", new String[]{id});
		
		finish();
		
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		if(dbHelper != null){
			dbHelper.close();
		}
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
